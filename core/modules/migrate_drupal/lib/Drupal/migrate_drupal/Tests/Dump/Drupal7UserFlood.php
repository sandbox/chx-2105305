<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Drupal7UserFlood.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing user.flood.yml migration.
 */
class Drupal7UserFlood extends Drupal6DumpBase {

  public function load() {
    $this->createTable('variable');
    $this->database->insert('variable')->fields(array(
      'name',
      'value',
    ))
    ->values(array(
      'name' => 'uid_only',
      'value' => "b:0;",
    ))
    ->values(array(
      'name' => 'ip_limit',
      'value' => "i:50;",
    ))
    ->values(array(
      'name' => 'ip_window',
      'value' => "i:3600;",
    ))
    ->values(array(
      'name' => 'user_limit',
      'value' => "i:5;",
    ))
    ->values(array(
      'name' => 'user_window',
      'value' => "i:21600;",
    ))
    ->execute();
  }
}
