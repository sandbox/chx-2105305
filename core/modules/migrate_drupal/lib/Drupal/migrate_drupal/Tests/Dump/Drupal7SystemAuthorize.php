<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Drupal7SystemAuthorizeSettings.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing system.authorize.yml migration.
 */
class Drupal7SystemAuthorize extends Drupal6DumpBase {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $this->createTable('variable');
    $this->database->insert('variable')->fields(array(
      'name',
      'value',
    ))
    ->values(array(
      'name' => 'authorize_filetransfer_default',
      'value' => serialize('ssh'),
    ))
    ->execute();
  }
}
