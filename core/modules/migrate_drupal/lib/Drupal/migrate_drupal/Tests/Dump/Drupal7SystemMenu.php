<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Drupal7SystemMenu.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing system.menu.yml migration.
 */
class Drupal7SystemMenu extends Drupal6DumpBase {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $this->createTable('variable');
    $this->database->insert('variable')->fields(array(
      'name',
      'value',
    ))
    ->values(array(
      'name' => 'menu_default_active_menus',
      'value' => serialize(array('SystemMenu' => 'TestSystemMenuMigration')),
    ))
    ->execute();
  }

}
