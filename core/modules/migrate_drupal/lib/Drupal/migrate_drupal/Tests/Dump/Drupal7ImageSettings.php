<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Drupal7ImageSettings.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing image.settings.yml migration.
 */
class Drupal7ImageSettings extends Drupal6DumpBase {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $this->createTable('variable');
    $this->database->insert('variable')->fields(array(
      'name',
      'value',
    ))
    ->values(array(
        'name' => 'allow_insecure_derivatives',
        'value' => "b:0;",
      ),
      array(
        'name' => 'suppress_itok_output',
        'value' => "b:0;",
      ))
    ->execute();
  }
  
}
