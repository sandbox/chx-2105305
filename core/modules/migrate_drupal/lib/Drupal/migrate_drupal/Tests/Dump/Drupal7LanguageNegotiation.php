<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\Dump\Drupal7LanguageNegotiation.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing language.negotiation.yml migration.
 */
class Drupal7LanguageNegotiation extends Drupal6DumpBase {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $this->createTable('variable');
    $this->database->insert('variable')->fields(array(
      'name',
      'value',
    ))
    ->values(array(
      'name' => 'locale_language_negotiation_session_param',
      'value' => serialize('language'),
    ))
    ->values(array(
      'name' => 'locale_language_negotiation_url_part',
      'value' => serialize('domain'),
    ))
    ->execute();
  }

}
