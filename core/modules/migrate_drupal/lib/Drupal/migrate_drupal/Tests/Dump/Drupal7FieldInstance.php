<?php

/**
 * @file
 * Contains \Drupal\migrate\Tests\Dump\Drupal7FieldInstance.
 */

namespace Drupal\migrate_drupal\Tests\Dump;

/**
 * Database dump for testing entity display migration.
 */
class Drupal7FieldInstance extends Drupal6DumpBase {

  /**
   * {@inheritdoc}
   */
  public function load() {
    foreach (static::getSchema() as $table => $schema) {
      // Create tables.
      $this->createTable($table, $schema);

      // Insert data.
      $data = static::getData($table);
      if ($data) {
        $query = $this->database->insert($table)->fields(array_keys($data[0]));
        foreach ($data as $record) {
          $query->values($record);
        }
        $query->execute();
      }
    }
  }

  /**
   * Defines schema for this database dump.
   *
   * @return array
   *   Associative array having the structure as is returned by hook_schema().
   */
  protected static function getSchema() {
    return array(
      'field_config' =>  array(
        'fields' => array(
          'id' => array(
            'type' => 'serial',
            'not null' => TRUE,
          ),
          'field_name' => array(
            'type' => 'varchar',
            'length' => 32,
            'not null' => TRUE,
          ),
          'type' => array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
          ),
          'module' => array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
            'default' => '',
          ),
          'active' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
          'storage_type' => array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
          ),
          'storage_module' => array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
            'default' => '',
          ),
          'storage_active' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
          'locked' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
          'data' => array(
            'type' => 'blob',
            'size' => 'big',
            'not null' => TRUE,
            'serialize' => TRUE,
          ),
          'cardinality' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
          'translatable' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
          'deleted' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
        ),
        'primary key' => array(
          'id',
        ),
        'indexes' => array(
          'field_name' => array(
            'field_name',
          ),
          'active' => array(
            'active',
          ),
          'storage_active' => array(
            'storage_active',
          ),
          'deleted' => array(
            'deleted',
          ),
          'module' => array(
            'module',
          ),
          'storage_module' => array(
            'storage_module',
          ),
          'type' => array(
            'type',
          ),
          'storage_type' => array(
            'storage_type',
          ),
        ),
      ),
      'field_config_instance' => array(
        'fields' => array(
          'id' => array(
            'type' => 'serial',
            'not null' => TRUE,
          ),
          'field_id' => array(
            'type' => 'int',
            'not null' => TRUE,
          ),
          'field_name' => array(
            'type' => 'varchar',
            'length' => 32,
            'not null' => TRUE,
            'default' => '',
          ),
          'entity_type' => array(
            'type' => 'varchar',
            'length' => 32,
            'not null' => TRUE,
            'default' => '',
          ),
          'bundle' => array(
            'type' => 'varchar',
            'length' => 128,
            'not null' => TRUE,
            'default' => '',
          ),
          'data' => array(
            'type' => 'blob',
            'size' => 'big',
            'not null' => TRUE,
            'serialize' => TRUE,
          ),
          'deleted' => array(
            'type' => 'int',
            'size' => 'tiny',
            'not null' => TRUE,
            'default' => 0,
          ),
        ),
        'primary key' => array(
          'id',
        ),
        'indexes' => array(
          'field_name_bundle' => array(
            'field_name',
            'entity_type',
            'bundle',
          ),
          'deleted' => array(
            'deleted',
          ),
        ),
      ),
    );
  }

  /**
   * Returns dump data from a specific table.
   *
   * @param string $table
   *   The table name.
   *
   * @return array
   *   Array of associative arrays each one having fields as keys.
   */
  public static function getData($table) {
    $data = array(
      'field_config' => array(
        array(
          'id' => '1',
          'field_name' => 'body',
          'type' => 'text_with_summary',
          'module' => 'text',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'entity_types' => array(
              'node',
            ),
            'translatable' => FALSE,
            'settings' => array(),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(
              'format' => array(
                'table' => 'filter_format',
                'columns' => array(
                  'format' => 'format',
                ),
              ),
            ),
            'indexes' => array(
              'format' => array(
                'format',
              ),
            ),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '3',
          'field_name' => 'field_test_two',
          'type' => 'number_integer',
          'module' => 'number',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => '1',
              'details' => array(
                'sql' => array(
                  'FIELD_LOAD_CURRENT' => array(
                    'field_data_field_test_two' => array(
                      'value' => 'field_test_two_value',
                    ),
                  ),
                  'FIELD_LOAD_REVISION' => array(
                    'field_revision_field_test_two' => array(
                      'value' => 'field_test_two_value',
                    ),
                  ),
                ),
              ),
            ),
            'foreign keys' => array(),
            'indexes' => array(),
            'id' => '3',
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '4',
          'field_name' => 'field_test_three',
          'type' => 'number_float',
          'module' => 'number',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(
              'decimal_separator' => '.',
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => '1',
              'details' => array(
                'sql' => array(
                  'FIELD_LOAD_CURRENT' => array(
                    'field_data_field_test_three' => array(
                      'value' => 'field_test_three_value',
                    ),
                  ),
                  'FIELD_LOAD_REVISION' => array(
                    'field_revision_field_test_three' => array(
                      'value' => 'field_test_three_value',
                    ),
                  ),
                ),
              ),
            ),
            'foreign keys' => array(),
            'indexes' => array(),
            'id' => '4',
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '5',
          'field_name' => 'field_test_link',
          'type' => 'link_field',
          'module' => 'link',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => FALSE,
            'entity_types' => array(),
            'settings' => array(
              'attributes' => array(
                'target' => 'default',
                'class' => '',
                'rel' => '',
              ),
              'url' => 0,
              'title' => 'optional',
              'title_value' => '',
              'title_maxlength' => 128,
              'enable_tokens' => 1,
              'display' => array(
                'url_cutoff' => 80,
              ),
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(),
            'indexes' => array(),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '6',
          'field_name' => 'field_test_filefield',
          'type' => 'file',
          'module' => 'file',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => FALSE,
            'entity_types' => array(),
            'settings' => array(
              'display_field' => 0,
              'display_default' => 0,
              'uri_scheme' => 'public',
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(
              'fid' => array(
                'table' => 'file_managed',
                'columns' => array(
                  'fid' => 'fid',
                ),
              ),
            ),
            'indexes' => array(
              'fid' => array(
                'fid',
              ),
            ),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '7',
          'field_name' => 'field_test_imagefield',
          'type' => 'image',
          'module' => 'image',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => FALSE,
            'entity_types' => array(),
            'settings' => array(
              'uri_scheme' => 'public',
              'default_image' => 0,
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(
              'fid' => array(
                'table' => 'file_managed',
                'columns' => array(
                  'fid' => 'fid',
                ),
              ),
            ),
            'indexes' => array(
              'fid' => array(
                'fid',
              ),
            ),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '8',
          'field_name' => 'field_test_phone',
          'type' => 'phone',
          'module' => 'phone',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => FALSE,
            'entity_types' => array(),
            'settings' => array(),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(),
            'indexes' => array(),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '9',
          'field_name' => 'field_test',
          'type' => 'text',
          'module' => 'text',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => FALSE,
            'entity_types' => array(),
            'settings' => array(
              'max_length' => 255,
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => 1,
            ),
            'foreign keys' => array(
              'format' => array(
                'table' => 'filter_format',
                'columns' => array(
                  'format' => 'format',
                ),
              ),
            ),
            'indexes' => array(
              'format' => array(
                'format',
              ),
            ),
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
        array(
          'id' => '10',
          'field_name' => 'field_test_datetime',
          'type' => 'datetime',
          'module' => 'date',
          'active' => '1',
          'storage_type' => 'field_sql_storage',
          'storage_module' => 'field_sql_storage',
          'storage_active' => '1',
          'locked' => '0',
          'data' => serialize(array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(
              'granularity' => array(
                'month' => 'month',
                'day' => 'day',
                'hour' => 'hour',
                'minute' => 'minute',
                'year' => 'year',
                'second' => 0,
              ),
              'tz_handling' => 'site',
              'timezone_db' => 'UTC',
              'cache_enabled' => 0,
              'cache_count' => '4',
              'todate' => '',
            ),
            'storage' => array(
              'type' => 'field_sql_storage',
              'settings' => array(),
              'module' => 'field_sql_storage',
              'active' => '1',
              'details' => array(
                'sql' => array(
                  'FIELD_LOAD_CURRENT' => array(
                    'field_data_field_test_datetime' => array(
                      'value' => 'field_test_datetime_value',
                    ),
                  ),
                  'FIELD_LOAD_REVISION' => array(
                    'field_revision_field_test_datetime' => array(
                      'value' => 'field_test_datetime_value',
                    ),
                  ),
                ),
              ),
            ),
            'foreign keys' => array(),
            'indexes' => array(),
            'id' => '10',
          )),
          'cardinality' => '1',
          'translatable' => '0',
          'deleted' => '0',
        ),
      ),
      'field_config_instance' => array(
        array(
          'id' => '1',
          'field_id' => '1',
          'field_name' => 'body',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Body',
            'widget' => array(
              'type' => 'text_textarea_with_summary',
              'settings' => array(
                'rows' => 20,
                'summary_rows' => 5,
              ),
              'weight' => '-4',
              'module' => 'text',
            ),
            'settings' => array(
              'display_summary' => TRUE,
              'text_processing' => 1,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'hidden',
                'type' => 'text_default',
                'settings' => array(),
                'module' => 'text',
                'weight' => 0,
              ),
              'teaser' => array(
                'label' => 'hidden',
                'type' => 'text_summary_or_trimmed',
                'settings' => array(
                  'trim_length' => 600,
                ),
                'module' => 'text',
                'weight' => 0,
              ),
            ),
            'required' => FALSE,
            'description' => '',
            'default_value' => array(
              array(
                'summary' => 'default summary text',
                'value' => 'default value text',
                'format' => 'basic_html',
              ),
            ),
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '3',
          'field_id' => '3',
          'field_name' => 'field_test_two',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Integer Field',
            'widget' => array(
              'weight' => '-2',
              'type' => 'number',
              'module' => 'number',
              'active' => 0,
              'settings' => array(),
            ),
            'settings' => array(
              'min' => 10,
              'max' => 100,
              'prefix' => 'pref',
              'suffix' => 'suff',
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'number_integer',
                'settings' => array(
                  'thousand_separator' => ' ',
                  'decimal_separator' => '.',
                  'scale' => 0,
                  'prefix_suffix' => TRUE,
                ),
                'module' => 'number',
                'weight' => 2,
              ),
            ),
            'required' => 1,
            'description' => '',
            'default_value' => NULL,
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '4',
          'field_id' => '4',
          'field_name' => 'field_test_three',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Float',
            'widget' => array(
              'type' => 'number',
              'weight' => '-1',
              'settings' => array(),
              'module' => 'number',
            ),
            'settings' => array(
              'min' => 0,
              'max' => 200,
              'prefix' => '',
              'suffix' => '',
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'number_decimal',
                'settings' => array(
                  'thousand_separator' => ' ',
                  'decimal_separator' => '.',
                  'scale' => 2,
                  'prefix_suffix' => TRUE,
                ),
                'module' => 'number',
                'weight' => 3,
              ),
            ),
            'default_value' => array(
              array(
                'value' => 20.3,
              ),
            ),
            'required' => FALSE,
            'description' => '',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '5',
          'field_id' => '5',
          'field_name' => 'field_test_link',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Test link',
            'widget' => array(
              'type' => 'link_field',
              'weight' => '0',
              'settings' => array(),
              'module' => 'link',
            ),
            'settings' => array(
              'attributes' => array(
                'target' => 'default',
                'class' => 'link-class',
                'rel' => 'nofollow',
              ),
              'url' => 0,
              'title' => 'optional',
              'title_value' => 'Click here',
              'title_maxlength' => 128,
              'enable_tokens' => 1,
              'display' => array(
                'url_cutoff' => 80,
              ),
              'validate_url' => 1,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'link_default',
                'settings' => array(),
                'module' => 'link',
                'weight' => 4,
              ),
            ),
            'required' => TRUE,
            'description' => '',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '6',
          'field_id' => '6',
          'field_name' => 'field_test_filefield',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Upload file',
            'widget' => array(
              'type' => 'file_generic',
              'weight' => '1',
              'settings' => array(
                'progress_indicator' => 'throbber',
              ),
              'module' => 'file',
            ),
            'settings' => array(
              'file_extensions' => 'txt pdf doc',
              'file_directory' => 'docs',
              'max_filesize' => '256 KB',
              'description_field' => TRUE,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'file_default',
                'settings' => array(),
                'module' => 'file',
                'weight' => 5,
              ),
            ),
            'required' => TRUE,
            'description' => 'Please upload a document.',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '7',
          'field_id' => '7',
          'field_name' => 'field_test_imagefield',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'Front view',
            'widget' => array(
              'type' => 'image_image',
              'weight' => '2',
              'settings' => array(
                'progress_indicator' => 'throbber',
                'preview_image_style' => 'thumbnail',
              ),
              'module' => 'image',
            ),
            'settings' => array(
              'file_extensions' => 'png gif jpg jpeg',
              'file_directory' => 'pictures',
              'max_filesize' => '2 MB',
              'alt_field' => 0,
              'title_field' => 0,
              'max_resolution' => '',
              'min_resolution' => '',
              'default_image' => NULL,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'image',
                'settings' => array(
                  'image_style' => '',
                  'image_link' => '',
                ),
                'module' => 'image',
                'weight' => 6,
              ),
            ),
            'required' => TRUE,
            'description' => 'The front view.',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '8',
          'field_id' => '8',
          'field_name' => 'field_test_phone',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'test_phone',
            'widget' => array(
              'type' => 'phone_textfield',
              'weight' => '3',
              'settings' => array(),
              'module' => 'phone',
            ),
            'settings' => array(
              'phone_country_code' => 0,
              'phone_default_country_code' => '1',
              'phone_int_max_length' => 15,
              'ca_phone_separator' => '-',
              'ca_phone_parentheses' => 1,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'phone',
                'settings' => array(),
                'module' => 'phone',
                'weight' => 7,
              ),
            ),
            'required' => FALSE,
            'description' => '',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '9',
          'field_id' => '9',
          'field_name' => 'field_test',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'test',
            'widget' => array(
              'type' => 'text_textfield',
              'weight' => '4',
              'settings' => array(
                'size' => 60,
              ),
              'module' => 'text',
            ),
            'settings' => array(
              'text_processing' => 0,
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'text_default',
                'settings' => array(),
                'module' => 'text',
                'weight' => 8,
              ),
            ),
            'required' => FALSE,
            'description' => '',
          )),
          'deleted' => '0',
        ),
        array(
          'id' => '10',
          'field_id' => '10',
          'field_name' => 'field_test_datetime',
          'entity_type' => 'node',
          'bundle' => 'story',
          'data' => serialize(array(
            'label' => 'test_datetime',
            'widget' => array(
              'type' => 'date_text',
              'weight' => '5',
              'settings' => array(
                'input_format' => 'm/d/Y - H:i:s',
                'input_format_custom' => '',
                'increment' => 15,
                'text_parts' => array(),
                'year_range' => '-3:+3',
                'label_position' => 'above',
              ),
              'module' => 'date',
            ),
            'settings' => array(
              'default_value' => 'now',
              'default_value_code' => '',
              'default_value2' => 'same',
              'default_value_code2' => '',
              'user_register_form' => FALSE,
            ),
            'display' => array(
              'default' => array(
                'label' => 'above',
                'type' => 'date_default',
                'settings' => array(
                  'format_type' => 'long',
                  'multiple_number' => '',
                  'multiple_from' => '',
                  'multiple_to' => '',
                  'fromto' => 'both',
                ),
                'module' => 'date',
                'weight' => 9,
              ),
            ),
            'required' => FALSE,
            'description' => '',
          )),
          'deleted' => '0',
        ),
      ),
    );

    return isset($data[$table]) ? $data[$table] : FALSE;
  }

}
