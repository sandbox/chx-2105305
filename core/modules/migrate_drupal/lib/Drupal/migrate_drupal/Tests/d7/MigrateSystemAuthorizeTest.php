<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateSystemAuthorizeTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\Plugin\migrate\destination\Null;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of authorize variables from the System module.
 */
class MigrateSystemAuthorizeTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('system');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name'  => 'Migrate variables to system.authorize.yml',
      'description'  => 'Upgrade variables to system.authorize.yml',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * Tests migration of authorize system settings variables to system.authorize.yml.
   */
  public function testSystemAuthorize() {
    $migration = entity_load('migration', 'd7_system_authorize');
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7SystemAuthorize.php',
    );
    $this->prepare($migration, $dumps);
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();
    $config = \Drupal::config('system.authorize');
    $this->assertIdentical($config->get('filetransfer_default'), 'ssh');
  }

}
