<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateUserFloodConfigTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of variables from the User flood module.
 */
class MigrateUserFloodConfigTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('user');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Migrate variables to user.flood.yml',
      'description' => 'Upgrade variables to user.flood.yml',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * Tests migration of user flood settings variables to user.flood.yml.
   */
  public function testUserFlood() {
    $migration = entity_load('migration', 'd7_user_flood');
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7UserFlood.php',
    );
    $this->prepare($migration, $dumps);
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();

    $config = \Drupal::config('user.flood');
    $this->assertIdentical($config->get('uid_only'), false);
    $this->assertIdentical($config->get('ip_limit'), 50);
    $this->assertIdentical($config->get('ip_window'), 3600);
    $this->assertIdentical($config->get('user_limit'), 5);
    $this->assertIdentical($config->get('user_window'), 21600);
  }
}
