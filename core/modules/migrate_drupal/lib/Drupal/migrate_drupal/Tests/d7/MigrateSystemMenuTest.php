<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateSystemMenuTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of variables from the system menu module.
 */
class MigrateSystemMenuTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('system');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name'  => 'Migrate variables to system.menu.yml',
      'description'  => 'Upgrade variables to system.menu.yml',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * Tests migration of system menu variables to system.menu.yml.
   */
  public function testSystemMenu() {
    $migration = entity_load('migration', 'd7_system_menu');
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7SystemMenu.php',
    );
    $this->prepare($migration, $dumps);
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();
    $config = \Drupal::config('system.menu');
    $this->assertIdentical($config->get('active_menus_default'), array('SystemMenu' => 'TestSystemMenuMigration'));
  }

}
