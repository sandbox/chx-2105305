<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateLanguageNegotiationTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of language negotiation variables.
 */
class MigrateLanguageNegotiationTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('language');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name'  => 'Migrate variables to language.negotiation.yml',
      'description'  => 'Upgrade variables to language.negotiation.yml',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * Tests migration of language negotiation variables to language.negotiation.yml.
   */
  public function testLanguageNegotiation() {
    $migration = entity_load('migration', 'd7_language_negotiation');
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7LanguageNegotiation.php',
    );
    $this->prepare($migration, $dumps);
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();
    $config = \Drupal::config('language.negotiation');
    $this->assertIdentical($config->get('session.parameter'), 'language');
    $this->assertIdentical($config->get('url.source'), 'domain');
  }

}
