<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateImageConfigTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateMessage;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of variables from the Image module.
 */
class MigrateImageConfigTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('image');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name'  => 'Migrate variables to image.*.yml',
      'description'  => 'Upgrade variables to image.*.yml',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * Tests migration of image settings variables to image.settings.yml.
   */
  public function testImageSettings() {
    $migration = entity_load('migration', 'd7_image_settings');
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7ImageSettings.php',
    );
    $this->prepare($migration, $dumps);
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();
    $config = \Drupal::config('image.settings');
    $this->assertIdentical($config->get('allow_insecure_derivatives'), FALSE);
    $this->assertIdentical($config->get('suppress_itok_output'), FALSE);
  }

}
