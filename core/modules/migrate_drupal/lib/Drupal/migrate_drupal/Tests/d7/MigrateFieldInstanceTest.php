<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Tests\d7\MigrateFieldInstanceTest.
 */

namespace Drupal\migrate_drupal\Tests\d7;

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate_drupal\Tests\MigrateDrupalTestBase;

/**
 * Tests migration of field instances.
 */
class MigrateFieldInstanceTest extends MigrateDrupalTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array(
    'telephone',
    'link',
    'file',
    'image',
    'datetime',
    'node',
  );

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Migrate D7 field instances to field.instance.*.*.*.yml',
      'description' => 'Migrate D7 field instances.',
      'group' => 'Migrate Drupal',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    /** @var \Drupal\migrate\entity\Migration $migration */
    $dumps = array(
      drupal_get_path('module', 'migrate_drupal') . '/lib/Drupal/migrate_drupal/Tests/Dump/Drupal7FieldInstance.php',
    );
    $this->loadDumps($dumps);

    // Create fields first.
    $migration = entity_load('migration', 'd7_field');
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();

    $migration = entity_load('migration', 'd7_field_instance');
    $executable = new MigrateExecutable($migration, $this);
    $executable->import();
  }

  /*
   * Tests migration of file variables to file.settings.yml.
   */
  public function testFieldInstanceSettings() {

    $entity = entity_create('node', array('type' => 'story'));

    // Text field.
    $field = entity_load('field_instance_config', 'node.story.body');
    $this->assertEqual($field->label(), 'Body');
    $expected = array('display_summary' => TRUE, 'text_processing' => 1);
    $this->assertEqual($field->getSettings(), $expected);
    $this->assertEqual('default summary text', $entity->body->summary);
    $this->assertEqual('default value text', $entity->body->value);

    // Integer.
    $field = entity_load('field_instance_config', 'node.story.field_test_two');
    $this->assertEqual($field->label(), 'Integer Field');
    $expected = array(
      'min' => '10',
      'max' => '100',
      'prefix' => 'pref',
      'suffix' => 'suff',
    );
    $this->assertEqual($field->getSettings(), $expected);
    $this->assertTrue($field->required);

    // Float.
    $field = entity_load('field_instance_config', 'node.story.field_test_three');
    $this->assertEqual($field->label(), 'Float');
    $expected = array(
      'min' => '0',
      'max' => '200',
      'prefix' => '',
      'suffix' => '',
      'decimal_separator' => '.',
    );
    $this->assertEqual($field->getSettings(), $expected);
    $this->assertEqual(20.3, $entity->field_test_three->value);
    $this->assertFalse($field->required);

    // Link.
    $field = entity_load('field_instance_config', 'node.story.field_test_link');
    $this->assertEqual($field->label(), 'Test link');
    $expected = array(
      'attributes' => array(
        'target' => 'default',
        'class' => 'link-class',
        'rel' => 'nofollow',
      ),
      'url' => 0,
      'title' => 'optional',
      'title_value' => 'Click here',
      'title_maxlength' => 128,
      'enable_tokens' => 1,
      'display' => array(
        'url_cutoff' => 80,
      ),
      'validate_url' => 1,
    );
    $this->assertEqual($field->getSettings(), $expected);
    $this->assertTrue($field->required);

    // File.
    $field = entity_load('field_instance_config', 'node.story.field_test_filefield');
    $this->assertEqual($field->label(), 'Upload file');
    $expected = array(
      'display_field' => FALSE,
      'display_default' => FALSE,
      'uri_scheme' => 'public',
      'file_extensions' => 'txt pdf doc',
      'file_directory' => 'docs',
      'max_filesize' => '256 KB',
      'description_field' => TRUE,
      'display_field' => FALSE,
      'display_default' => FALSE,
      'target_type' => 'file',
    );
    $this->assertEqual($field->getSettings(), $expected);
    $this->assertEqual('Please upload a document.', $field->description);
    $this->assertTrue($field->required);

    // Image.
    $field = entity_load('field_instance_config', 'node.story.field_test_imagefield');
    $this->assertEqual($field->label(), 'Front view');
    $default_image = array(
      'fid' => NULL,
      'alt' => '',
      'title' => '',
      'width' => NULL,
      'height' => NULL,
    );
    $settings = $field->getSettings();
    $this->assertEqual($settings['file_extensions'], 'png gif jpg jpeg');
    $this->assertEqual($settings['file_directory'], 'pictures');
    $this->assertEqual($settings['default_image'], $default_image);
    $this->assertEqual('The front view.', $field->description);
    $this->assertTrue($field->required);
  }

}
