<?php

/**
 * @file
 * Contains \Drupal\migrate_drupal\Plugin\migrate\source\File.
 */

namespace Drupal\migrate_mtm\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 file source from database.
 *
 * @MigrateSource(
 *   id = "d7_file"
 * )
 */
class File extends DrupalSqlBase {

  /**
   * The file directory path.
   *
   * @var string
   */
  protected $filePath;

  /**
   * Flag for private or public file storage.
   *
   * @var boolean
   */
  protected $isPublic;

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('file_managed', 'f')->fields('f', array(
      'fid',
      'uid',
      'filename',
      'uri',
      'filemime',
      'filesize',
      'status',
      'timestamp',
      'type'
    ));
    return $query;
  }


  /**
   * {@inheritdoc}
   */
  protected function runQuery() {
    $conf_path = isset($this->configuration['conf_path']) ? $this->configuration['conf_path'] : 'sites/default';
    $this->filePath = $this->variableGet('file_directory_path', $conf_path . '/files') . '/';

    // FILE_DOWNLOADS_PUBLIC == 1 and FILE_DOWNLOADS_PRIVATE == 2.
    $this->isPublic = $this->variableGet('file_downloads', 1) == 1;
    return parent::runQuery();
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return array(
      'fid' => $this->t('File ID'),
      'uid' => $this->t('The {users}.uid who added the file. If set to 0, this file was added by an anonymous user.'),
      'filename' => $this->t('File name'),
      'uri' => $this->t('File URI'),
      'filemime' => $this->t('File Mime Type'),
      'status' => $this->t('The published status of a file.'),
      'created' => $this->t('The time that the file was added.'),
      'changed' => $this->t('The time that the file was last changed.'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['fid']['type'] = 'integer';
    return $ids;
  }

}
