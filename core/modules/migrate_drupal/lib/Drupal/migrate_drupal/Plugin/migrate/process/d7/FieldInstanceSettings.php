<?php

/**
 * @file
 * Contains Drupal\migrate_drupal\Plugin\migrate\d7\FieldInstanceSettings
 */

namespace Drupal\migrate_drupal\Plugin\migrate\Process\d7;

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_field_instance_settings"
 * )
 */
class FieldInstanceSettings extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   *
   * Set the field instance defaults.
   */
  public function transform($value, MigrateExecutable $migrate_executable, Row $row, $destination_property) {
    list($instance_settings, $widget_settings) = $value;
    $widget_type = $widget_settings['type'];

    $settings = array();
    switch ($widget_type) {
      case 'image_image':
        $settings = $instance_settings;
        $settings['default_image'] = array(
          'fid' => $instance_settings['default_image'],
          'alt' => '',
          'title' => '',
          'width' => NULL,
          'height' => NULL,
        );
        break;

      // @todo Add here other special conversions, if case.

      default:
        $settings = $instance_settings;
    }

    // Remove 'user_register_form'.
    // @see https://drupal.org/node/2049485'
    unset($settings['user_register_form']);

    return $settings;
  }

}
