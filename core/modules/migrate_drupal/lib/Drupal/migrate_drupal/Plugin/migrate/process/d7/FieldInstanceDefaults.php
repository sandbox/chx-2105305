<?php

/**
 * @file
 * Contains Drupal\migrate_drupal\Plugin\migrate\d7\FieldInstanceDefaults
 */

namespace Drupal\migrate_drupal\Plugin\migrate\Process\d7;

use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_field_instance_defaults"
 * )
 */
class FieldInstanceDefaults extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutable $migrate_executable, Row $row, $destination_property) {
    list($default_value, $widget_settings) = $value;
    $widget_type = $widget_settings['type'];

    $default = array();

    foreach ($default_value as $item) {
      switch ($widget_type) {
        // @todo Add here special conversion, if case

        default:
          $default[] = $item;
      }
    }

    return $default;
  }

}
