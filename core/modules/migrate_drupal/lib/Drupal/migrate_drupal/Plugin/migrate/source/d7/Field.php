<?php

/**
 * @file
 * Contains \Drupal\migrate\Plugin\migrate\source\d6\Field.
 */

namespace Drupal\migrate_drupal\Plugin\migrate\source\d7;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 field source from database.
 *
 * This source class doesn't really work for anything but the Drupal 8
 * migration because it returns only active fields, one per entity type and
 * field name.
 *
 * @MigrateSource(
 *   id = "d7_field"
 * )
 */
class Field extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('field_config', 'fc')
      ->fields('fc', array(
        'field_name',
        'type',
        'module',
        'storage_type',
        'storage_module',
        'locked',
        'data',
        'cardinality',
        'translatable',
      ))
      ->fields('fci', array(
        'entity_type',
      ))
      ->condition('fc.active', 1)
      ->condition('fc.deleted', 0)
      ->condition('fc.storage_active', 1);
    $query->join('field_config_instance', 'fci', 'fc.id = fci.field_id');
    $query->orderBy('fc.field_name');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return array(
      'field_name' => $this->t('The name of this field.'),
      'type' => $this->t('The type of this field.'),
      'module' => $this->t('The module that implements the field type.') ,
      'storage' => $this->t('The field stoerage.'),
      'locked' => $this->t( 'Locked'),
      'cardinality' => $this->t('Cardinality'),
      'translatable' => $this->t('translatable'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row, $keep = TRUE) {
    // The logic is from field_read_fields() in Drupal 7.
    $field = unserialize($row->getSourceProperty('data'));
    $field['field_name'] = $row->getSourceProperty('field_name');
    $field['type'] = $row->getSourceProperty('type');
    $field['module'] = $row->getSourceProperty('module');
    $field['storage']['type'] = $row->getSourceProperty('storage_type');
    $field['storage']['module'] = $row->getSourceProperty('storage_module');
    $field['locked'] = $row->getSourceProperty('locked');
    $field['cardinality'] = $row->getSourceProperty('cardinality');
    $field['translatable'] = $row->getSourceProperty('translatable');
    foreach ($field as $key => $data) {
      $row->setSourceProperty($key, $data);
    }
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['field_name']['type'] = 'string';
    $ids['field_name']['alias'] = 'fc';
    $ids['entity_type']['type'] = 'string';
    $ids['entity_type']['alias'] = 'fci';
    return $ids;
  }
}
